import numpy as np
import matplotlib.pyplot as plt
import cv2 as cv

image = cv.imread ("Lena.png") # charge le fichier dans une matrice de pixels gris
mbappe = cv.imread ("MBAPPE-CANAL.png") # charge le fichier dans une matrice de pixels gris


def negatif(image):
    imageNegatif = 255 - image
    plt.imshow ( imageNegatif[... , :: -1])
    plt.show()

def luminosite(image,int):
    imageLuminosite = cv.convertScaleAbs(image, beta=int)
    plt.imshow(imageLuminosite[...,:: -1])
    plt.show()

def redimension(image,scale):
    width = int(image.shape[1] * scale)
    height = int(image.shape[0] * scale)
    dim = (width, height)
    imageResize = cv.resize(image,dim, interpolation = cv.INTER_AREA)
    plt.imshow(imageResize[... , :: -1])
    plt.show()

def rotate90(image):
    image90 = cv.rotate(image, cv.ROTATE_90_CLOCKWISE)
    plt.imshow(image90[... , :: -1])
    plt.show()

def rotate180(image):
    image180 = cv.rotate(image, cv.ROTATE_180)
    plt.imshow(image180[... , :: -1])
    plt.show()

def rotate270(image):
    image270 = cv.rotate(image, cv.ROTATE_90_COUNTERCLOCKWISE)
    plt.imshow(image270[... , :: -1])
    plt.show()


negatif(mbappe)
luminosite(mbappe,-50)
redimension(mbappe,2)
rotate90(mbappe)
rotate180(mbappe)
rotate270(mbappe)